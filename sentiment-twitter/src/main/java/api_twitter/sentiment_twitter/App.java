package api_twitter.sentiment_twitter;


import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws TwitterException
    {
        Twitter twitter = TwitterFactory.getSingleton();
    	Query query = new Query();
    	query.lang("es");
    	query.query("no senti temblor");
    	query.count(100);
    	QueryResult result = twitter.search(query);
    	for (Status status : result.getTweets()) {
    	     System.out.println("@" + status.getUser().getScreenName() + ":" + status.getText());
    	}
    }
}
